﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MyGame/Create Player Stats Object")]
public class PlayerClass : ScriptableObject
{
    public new string name;
    public int maxHealth;
}
