﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    [Header("Properties")]
    [SerializeField] PlayerClass _class;
    [SerializeField] Weapon _equippedWeapon;

    [Header("Visuals")]
    [SerializeField] TMP_Text classNameText;
    [SerializeField] TMP_Text classHealthText;
    [SerializeField] TMP_Text weaponNameText;
    [SerializeField] TMP_Text weaponDamageText;
    [SerializeField] TMP_Text weaponDurabilityText;
    [SerializeField] TMP_Text weaponIsProjectileText;
    [SerializeField] TMP_Text weaponRangeText;

    public Weapon EquippedWeapon
    {
        get
        {
            return _equippedWeapon;
        }
        set
        { 
            _equippedWeapon = value;
            UpdateWeaponVisuals();
        }
    }

    public PlayerClass Class
    {
        get
        {
            return _class;
        }

        set
        {
            _class = value;
            UpdateClassVisuals();
        }
    }

    private void Start()
    {
        UpdateClassVisuals();
        UpdateWeaponVisuals();
    }

    void UpdateWeaponVisuals()
    {
        weaponNameText.text = "Name: " + _equippedWeapon.name;
        weaponDamageText.text = "Damage: " + _equippedWeapon.damage.ToString();
        weaponDurabilityText.text = "Durability: " + _equippedWeapon.durability.ToString();
        weaponIsProjectileText.text = "Is Projectile: " + _equippedWeapon.isProjectile.ToString();
        weaponRangeText.text = "Range: " + _equippedWeapon.range.ToString();
    }

    void UpdateClassVisuals()
    {
        classNameText.text = "Name: " + _class.name;
        classHealthText.text = "Max Health: " + _class.maxHealth.ToString();
    }
}
