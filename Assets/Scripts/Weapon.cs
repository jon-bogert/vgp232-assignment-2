using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MyGame/Create Weapon Object")]
public class Weapon : ScriptableObject
{
    public new string name;
    public int damage;
    public int durability;
    public bool isProjectile;
    public float range;
}
