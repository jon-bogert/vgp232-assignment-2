﻿using UnityEngine;

public class PlayerLoadoutSwitcher : MonoBehaviour
{
    [Header("Scriptable Objects")]
    [SerializeField] PlayerClass[] playerClasses;
    [SerializeField] Weapon[] weapons;

    KeyCode nextClass = KeyCode.Equals;
    KeyCode prevClass = KeyCode.Minus;
    KeyCode nextWeapon = KeyCode.RightBracket;
    KeyCode prevWeapon = KeyCode.LeftBracket;

    [Header("Optional References")]
    [SerializeField] Player player = null;

    int weaponIndex = 0;
    int classIndex = 0;

    private void Start()
    {
        if (player == null)
            player = FindObjectOfType<Player>();

        if (player == null)
            Debug.LogWarning("PlayerLoadoutSwitcher -> Could not find instance of Player");

        if (playerClasses == null || (playerClasses != null && playerClasses.Length == 0))
            Debug.LogWarning("PlayerLoadoutSwitcher -> Add Classes to inspector");
        if (weapons == null || (weapons != null && weapons.Length == 0))
            Debug.LogWarning("PlayerLoadoutSwitcher -> Add Weapons to inspector");
    }

    private void Update()
    {
        if (player == null) return;

        if (Input.GetKeyDown(nextClass))
        {
            classIndex = (classIndex + 1) % playerClasses.Length;
            player.Class = playerClasses[classIndex];
        }
        else if (Input.GetKeyDown(prevClass))
        {
            if (--classIndex < 0) classIndex = playerClasses.Length - 1;
            player.Class = playerClasses[classIndex];
        }

        if (Input.GetKeyDown(nextWeapon))
        {
            weaponIndex = (weaponIndex + 1) % weapons.Length;
            player.EquippedWeapon = weapons[weaponIndex];
        }
        else if (Input.GetKeyDown(prevWeapon))
        {
            if (--weaponIndex < 0) weaponIndex = weapons.Length - 1;
            player.EquippedWeapon = weapons[weaponIndex];
        }
    }
}
